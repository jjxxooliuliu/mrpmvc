<?php if(!defined('IN_APP')){exit();}?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MrPmvc模型生成工具</title>
<style type="text/css">
body{padding:40px;}
th,td{padding:10px;}
legend{color:green;font-size:16px;font-weight:bold;}
.hr1{border-bottom:2px solid #A6C61C;margin:0;padding:0;}
fieldset{border:2px solid #cdcdcd;margin-bottom:20px;}
input[type=button]{cursor:pointer;}
td{text-align:center;}
</style>
[{script('js/jq/j')}]
<script>
function dosubmit(){
    table=$('#table').val();
    model=$('#model').val();
    pk=$('#pk').val();
     if(!table||!model||!pk){
           alert('请输入表名,主键名和类名.');
           return false;
         }
     return true;
}
</script>
</head>
<body>
<fieldset>
<legend>读取模型信息</legend>
<hr class="hr1"/>
<form id="form" action="?c=model_builder.Index&m=ReadInfo" method="post" onsubmit="return dosubmit();">
<table cellpadding="0" cellspacing="0">
<tr><td>表名：</td><td><input type="text" id="table" name="table" value="[{P('table');}]"/></td><td>&nbsp;</td></tr>
<tr><td>表主键名：</td><td><input type="text" id="pk" name="pk" value="[{P('pk');}]"/></td><td>&nbsp;</td></tr>
<tr><td>模型类名:</td><td><input type="text" id="model" name="model" value="[{P('model');}]"/></td><td>Model.class.php</td></tr>
<tr><td colspan="3" align="center"><input type="button" onclick="$('#form').submit();" value=" 读取信息 "/></td></tr>
</table>
</form>
</fieldset>
</body>
</html>