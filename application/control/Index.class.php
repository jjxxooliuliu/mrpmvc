<?php  if(!defined('IN_APP')){exit();}
#默认控制器
class Index extends BaseControl{
      public function __construct(){
            parent::__construct();#一定不能忘记调用父类构造方法
            $this->load('Test');
      }
      public function onIndex(){
       $data=$this->model->Test->page(5);
       $this->view->display('index/index.php',$data);
       $this->db->debug();
      }
      public function onTest(){
           #################################################
             $_POST['id']='123';
             $_POST['name']='修改';
             if($this->model->Test->update()){
                    echo '修改成功<br/>';
             }elseif($this->model->Test->msg){
                    echo $this->model->Test->msg.'<br/>';
             }else{
                    echo '修改失败<br/>';
             }
             ##############################################
             $_POST['id']='23';
             $_POST['name']='修改'.rand();
             if($this->model->Test->update()!==false){
                    echo '修改成功 name:'.$_POST['name'].'<br/>';
             }elseif($this->model->Test->msg){
                    echo $this->model->Test->msg.'<br/>';
             }else{
                    echo '修改失败<br/>';
             }
             ##############################################
             $_POST['id']='';
             $_POST['name']='修改';
             if($this->model->Test->update()!==false){
                    echo '修改成功<br/>';
             }elseif($this->model->Test->msg){
                    echo $this->model->Test->msg.'<br/>';
             }else{
                    echo '修改失败<br/>';
             }
             #################################################
             $_POST['id']='';
             $_POST['name']='';
             if($this->model->Test->insert()){
                    echo '添加成功<br/>';
             }elseif($this->model->Test->msg){
                    echo $this->model->Test->msg.'<br/>';
             }else{
                    echo '添加失败<br/>';
             }
             ##############################################
             unset($_POST['id']);
             $_POST['name']='添加'.rand();
             $id=$this->model->Test->insert();
             if($id){
                    echo '添加成功ID:'.$id.'<br/>';
             }elseif($this->model->Test->msg){
                    echo $this->model->Test->msg.'<br/>';
             }else{
                    echo '添加失败<br/>';
             }
             ##############################################
      
      }
}