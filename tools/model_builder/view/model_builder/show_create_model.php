<?php if(!defined('IN_APP')){exit();}?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MrPmvc模型生成工具</title>
[{script('js/jq/j')}]
<script>
$(function(){
    var $form=$('#data_form');
     $('#createModel').click(function(){
         $form.attr({'target':'hide','action':'?c=model_builder.Index&m=CreateModel'});
         $form.submit();
     });
      $('#next').click(function(){
         $form.attr({'target':'_self','action':'?c=model_builder.Index&m=ShowCreateAction'});
         $form.submit();
      });
});
</script>
<style type="text/css">
body{padding:40px;}
th,td{padding:10px;}
legend{color:green;font-size:16px;font-weight:bold;}
fieldset{border:2px solid #cdcdcd;margin-bottom:20px;}
.hr1{border-bottom:2px solid #A6C61C;margin:0;padding:0;}
input[type=button]{cursor:pointer;}
td{text-align:center;}
</style>
</head>
<body>
<form id="data_form" action="" method="post">
<fieldset>
<legend>添加数据规则</legend>
<hr class="hr1"/>
<table cellpadding="0" cellspacing="0" >
<tr><th>字段名</th><th>验证规则（正则表达式或模型方法名）</th><th>验证失败提示信息</th></tr>
<?php foreach($rule_add  as $col){?>
<tr>
    <td>[{$col}]</td>
    <td><input type="text" id="[{$col}]_add_reg" name="[{$col}]_add_reg"/></td></td>
    <td><input type="text" id="[{$col}]_add_hint" name="[{$col}]_add_hint"/></td></td>
</tr>
<?php }//end foreach?>
</table>
</fieldset>
<fieldset>
<legend>修改数据规则(留空则与上面相同)</legend>
<table cellpadding="0" cellspacing="0" >
<tr><th>字段名</th><th>验证规则（正则表达式或模型方法名）</th><th>验证失败提示信息</th></tr>
<?php foreach($rule_modify  as $col){?>
<tr>
    <td>[{$col}]</td>
    <td><input type="text" id="[{$col}]_modify_reg" name="[{$col}]_modify_reg"/></td></td>
    <td><input type="text" id="[{$col}]_modify_hint" name="[{$col}]_modify_hint"/></td></td>
</tr>
<?php }//end foreach?>
</table>
</fieldset>
<?php foreach($attach as $key=>$value){?>
<input type="hidden" value="[{$value}]" name="[{$key}]"/>
<?php }//end foreach?>
</form>
<fieldset>
<legend>生成操作</legend>
<table cellpadding="0" cellspacing="0" >
<tr><td><input type="button" id="createModel" value="1.生成模型" style="font-weight:bolder;color:red;font-size: 16px;"/>
    <input type="button" id="next" value="2.下一步"/>
    </td></tr>
</table>
</fieldset>
    <iframe name="hide" style="display:none;"></iframe>
</body>
</html>