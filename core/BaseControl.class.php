<?php if(!defined('IN_APP')){exit();}
/**
 * MrPmvc基础控制器
 * @author 狂奔的蜗牛
 * @email  672308444@163.com
 * @version alpha
 */
class BaseControl extends BaseUtil{
      protected $view;
      protected $model;
      public function __construct(){
              parent::__construct();
              #初始化模型容器，方便在控制器中通过$this->model->模型类名称 来访问加载的模型对象。
              $this->model=new BaseModel();
              #初始化视图，优先使用接口返回的自定义视图对象
              $view=BaseInterface::getViewOBJ($this->getConfig());
              $this->view=is_object($view)?$view:new BaseView();
      }
      public function load($model,$modelName=null,$control=null){
          $control=$control?$control:$this;
          eval('$modelObj= new '.$model.'Model();$modelObj->setControl($control);');
          $this->model->add_obj($modelObj,$modelName);
           return $modelObj;
      }
}