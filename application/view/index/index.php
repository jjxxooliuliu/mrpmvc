<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>列表展示_MrPmvc框架Demo</title>
[{script('js/jq/j')}]
</head>
<body>
<fieldset>
<legend>列表展示（SQLite3数据库文件位于res/spider.db3）</legend>
<table cellpadding="0" cellspacing="0">
    <?php foreach($rows as $row){?>
    <tr><td width="50">ID</td>
        <td width="50"><a target="_blank" href="<?php echo ABS_ENTRANCE_URL?>?c=Test&m=Modify&id=[{$row['id']}]">[{$row['id']}]</a></td>
        <td width="50">姓名</td>
        <td>[{$row['name']}]</td>
    </tr>
    <?php }//end foreach?>
</table>
    <p>[{$navhtml}]</p>
</fieldset>
    <p><a target="_blank" href="<?php echo ABS_ENTRANCE_URL?>?c=Test&m=Add">添加数据</a></p>
    <p>项目地址:<a href="http://sourceforge.net/projects/mrpmvc/">http://sourceforge.net/projects/mrpmvc/</a></p>
</body>
</html>