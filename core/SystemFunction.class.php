<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SystemFunction
 *
 * @author Administrator
 */
class SystemFunction {

    public static $starttime;

    static function markStart() {
        #运行时间统计 
        $mtime = explode(' ', microtime());
        self::$starttime = $mtime[1] + $mtime[0];
    }

    static function init() {
        #加载系统核心函数库
        require(SYS_ROOT . 'BaseFunctions.php');
        require(SYS_ROOT . 'BaseInterface.class.php');
        require(SYS_ROOT . 'BaseUtil.class.php');
        require(SYS_ROOT . 'BaseModel.class.php');
        require(SYS_ROOT . 'BaseView.class.php');
        require(SYS_ROOT . 'BaseControl.class.php');
        require(SYS_ROOT . 'BaseDB.class.php');
        require(SYS_ROOT . 'Cache.class.php');
        tryCreateDir(RESOURCE_LINK);
        tryCreateDir(CACHE_DIR);
        tryCreateDir(SITE_LOG_SAVE_PATH);
        #清除所有的魔法转义
        stripslashes_all();
        spl_autoload_register(array('SystemFunction', 'appAutoload'));
        self::loadClass();
    }

    static function loadClass() {
        $method = isset($_GET['m']) ? $_GET['m'] : DEFAULT_CONTROL_METHOD;
        $control = isset($_GET['c']) ? $_GET['c'] : DEFAULT_CONTROL;
        $control_name_a = explode('.', $control);
        $control = str_replace('.', '/', $control);
        $file = APP_ROOT . 'control/' . $control . '.class.php';
        $control_clazz_name = $control_name_a[count($control_name_a) - 1];
        if (file_exists($file)) {
            include_once($file);
            $c = new $control_clazz_name();
            if (method_exists($c, 'on' . $method)) {
                $obstart = COUNT_RUNTIME || method_exists($c, '__output');
                if ($obstart) {#输出控制是否开启
                    ob_start();
                }
                define('ACTION_NAME', $control); #定义当前控制器名称
                define('MEHTOD_NAME', $method); #定义当前方法名称
                $c->{'on' . $method}();
                $mtime = explode(' ', microtime());
                $runtime = sprintf('%.3f', $mtime[1] + $mtime[0] - self::$starttime);
                $html = '';
                #运行统计
                if (COUNT_RUNTIME) {
                    $html = str_replace('{RUNTIME}', $runtime, ob_get_contents());      #运行时间,单位秒
                    $html = str_replace('{MEMORY}', memory_get_usage(TRUE) / 1024, $html); #运行内存消耗，单位KB
                    ob_clean();
                }
                #控制器默认输出判断
                if (method_exists($c, '__output')) {
                    ob_clean();
                    $c->__output($html);
                } else {
                    echo($html);
                }
                #网站访问日志记录
                siteLog(SITE_LOG_IS_STOP);
            } else {
                notFound('Method : <b>' . $method . '</b>');
            }
        } else {
            notFound('Class : <b>' . $control . '</b>');
        }
    }

    public static function appAutoload($clazzName) {
        $model = APP_ROOT . 'model/' . $clazzName . '.class.php';
        $library = APP_ROOT . 'library/' . $clazzName . '.class.php';
        if (file_exists($library)) {
            include($library);
        } elseif (file_exists($model)) {
            include($model);
        } else {
            #有大于1个的autoload吗？这里判断一下，避免干扰其它autoload
            if (count(spl_autoload_functions()) > 1) {
                return; #有大于一个的autoload直接返回，让其它的autoload继续查找。
            } else {
                #只有一个autoload即本MrPmvc的appAutoload，那么就做404提示处理
                echo notFound('Class : ' . $clazzName);
            }
        }
    }

}
